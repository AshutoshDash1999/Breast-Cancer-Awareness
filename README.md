# Breast-Cancer-Awareness
A website for spreading awareness against Breast Cancer.

# Deployement 
Site is hosted [here](https://ashutoshdash1999.github.io/Breast-Cancer-Awareness/)

# Screencast
A sample view of the site:

![Screencast from 01-10-20 07 59 29 AM IST](https://user-images.githubusercontent.com/46455250/94759142-c2b04900-03bc-11eb-9448-5945576bd5f4.gif)
